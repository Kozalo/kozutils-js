"use strict";

/**
 * @namespace KozExceptions
 * @desc Set of some useful exceptions.
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 */
let KozExceptions = {

	/**
     * Returns a complete string to throw an exception.
     * @memberof KozExceptions
     * @private
     * @ignore
     * @param {string} error The type of an exception.
     * @param {string=|Object=} argument
     * @param {string=} message
     * @returns {string}
     */
	_genericException: function(error, argument, message) {
		if (error === undefined)
			throw '[Exception module] An internal error has occured (the type of an error is undefined)!';

		this.error = error;
		this.argument = (argument instanceof Object) ? JSON.stringify(argument) : argument;
		this.message = message;
	},


	/**
	 * Throw this exception when you encountered any invalid argument.
	 * @memberof KozExceptions
	 * @param {string=|Object=} argument String or any object. You can pass here, for example, the name of the argument, or an object consisting of the name and value.
	 * @param {string=} message Any string you want.
	 */
	invalidArgument: function(argument, message) { KozExceptions._genericException.call(this, 'Invalid argument', argument, message); },

	/**
	 * Throw this exception when the user code didn't pass you any mandatory argument.
	 * @memberof KozExceptions
	 * @param {string=|Object=} argument String or any object. You can pass here, for example, the name of the argument, or an object consisting of the name and value.
	 * @param {string=} message Any string you want.
	 */
	missingArgument: function(argument, message) { KozExceptions._genericException.call(this, 'Missing argument', argument, message); },

	/**
	 * Just a stub for your code you plan to implement in the future.
	 * @memberof KozExceptions
	 */
	notImplementedYet: function() { KozExceptions._genericException.call(this, 'Not implemented yet'); },

	/**
	 * Throw this exception when anyone tries to create an instance of your abstract class.
	 * @memberof KozExceptions
	 */
	abstractClass: function() { KozExceptions._genericException.call(this, 'You cannot create an instance of the abstract class'); },


	/**
	 * This exception is supposed to be thrown when you check the existence of an object or function from any external library, which expected to be in the scope, and suddenly you find out that it doesn't exist there.
	 * @memberof KozExceptions
	 * @param {string=} library Name of the missing library.
	 */
	unresolvedDependency: function(library) { KozExceptions._genericException.call(this, "Couldn't find a required library", library); }

};

KozExceptions._genericException.prototype.toString = function() {
	if (this.argument === undefined)
		return this.error + '!';
	else if (this.message === undefined)
		return this.error + ': ' + this.argument + '!';
	else
		return this.error + ': ' + this.argument + '! ' + this.message;
}

// JavaScript OOP is so strange...
for (let key in KozExceptions) {
	if (KozExceptions[key] === KozExceptions._genericException)
		continue;

	KozExceptions[key].prototype = Object.create(KozExceptions._genericException.prototype);
	KozExceptions[key].prototype.constructor = KozExceptions[key];
}
