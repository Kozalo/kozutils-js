KozUtils
=========

### Для русскоязычных людей

Для вас у меня есть даже более [подробный мануал](http://kozalo.ru/#post-1482074368) по библиотеке. Но автоматически сгенерированная документация всё же только на моём кривом английском.  


### Introduction

This library has several static functions to make your life as a JavaScript developer a little bit easier.  
Also, it has a collection of some useful exceptions. Finally, it has extension functions to the String class and functions for working with YouTube.  


### Overview

Look at the left sidebar. There are several functions there:  
1. [KozUtils.initOptions()](KozUtils.html#.initOptions) — Do you use an object to pass parameters to a function? And also you have an object with default parameters for a user to override them? Excellent! This function is exactly the thing you're looking for your entire life! Let it pass the options for you!  
2. [KozUtils.checkMissingParameters()](KozUtils.html#.checkMissingParameters) — Do you cry into your pillow every night because of incapacity of JavaScript to check whether parameters exist or they are undefined? OK. That's OK! We all do. Fortunately, I have one useful function special for you!  
3. [KozUtils.checkParameters()](KozUtils.html#.checkParameters) — Unlike the previous function, not only checks the presence of arguments but also ensures correctness of types of all arguments.  
4. [KozUtils.getActualSize()](KozUtils.html#.getActualSize) — Use this method when you want to get the real current size of any HTML element.  

KozExceptions:  
1. [KozExceptions.invalidArgument()](KozExceptions.html#.invalidArgument) — Throw it if you got an invalid argument.  
2. [KozExceptions.missingArgument()](KozExceptions.html#.missingArgument) — Throw it if required argument is undefined.  
3. [KozExceptions.notImplementedYet()](KozExceptions.html#.notImplementedYet) — Throw it if you need a stub for a not implemented method.  
4. [KozExceptions.abstractClass()](KozExceptions.html#.abstractClass) — Throw it within the constructor of any abstract class.  
5. [KozExceptions.unresolvedDependency()](KozExceptions.html#.unresolvedDependency) — Throw it if you encountered the absence of any required library.  

String extensions:  
1. [matchAll()](String.html#.matchAll) — Uses your regular expression to find matches in the string.  
2. [shortenTo()](String.html#.shortenTo) — Shorten your string to the closest whole word before _length_ characters.  

YouTube functions:  
1. [KozYouTubeUtils.parseId()](KozYouTubeUtils.html#.parseId) — Parses received text and returns only the ID of the first found video. Returns false if found nothing.  
2. [KozYouTubeUtils.durationToString()](KozYouTubeUtils.html#.durationToString) — Converts "PT3M54S" to "03:54".  
