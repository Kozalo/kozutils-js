"use strict";

describe("Tests for KozYouTubeUtils", function() {

	describe("ParseId", function() {

		describe("Parse long links", function() {

            it("Full URL", function() {
                var url = "https://www.youtube.com/watch?v=tKTZoB2Vjuk";
                var result = KozYouTubeUtils.parseId(url);
                expect(result).toBe("tKTZoB2Vjuk");
            });

            it("URL without WWW and SSL", function() {
                var url = "http://youtube.com/watch?v=tKTZoB2Vjuk";
                var result = KozYouTubeUtils.parseId(url);
                expect(result).toBe("tKTZoB2Vjuk");
            });

            it("URL with video ID containing a dash", function() {
                var url = "https://www.youtube.com/watch?v=S-RjiMAxHio";
                var result = KozYouTubeUtils.parseId(url);
                expect(result).toBe("S-RjiMAxHio");
            });

            it("Full URL with timing", function() {
                var url = "https://www.youtube.com/watch?v=Osck3M7ZvPc&t=7m12s";
                var result = KozYouTubeUtils.parseId(url);
                expect(result).toBe("Osck3M7ZvPc");
            });

            it("Full URL with timing before", function() {
                var url = "https://www.youtube.com/watch?time=1s&v=S-RjiMAxHio";
                var result = KozYouTubeUtils.parseId(url);
                expect(result).toBe("S-RjiMAxHio");
            });

        });


        describe("Parse short links", function() {

            it("URL", function() {
                var url = "https://youtu.be/3zWwd8n2JVI";
                var result = KozYouTubeUtils.parseId(url);
                expect(result).toBe("3zWwd8n2JVI");
            });

            it("URL without SSL", function() {
                var url = "http://youtu.be/3zWwd8n2JVI";
                var result = KozYouTubeUtils.parseId(url);
                expect(result).toBe("3zWwd8n2JVI");
            });

            it("URL with video ID containing a dash", function() {
                var url = "https://youtu.be/a-GQDCtt1Vk";
                var result = KozYouTubeUtils.parseId(url);
                expect(result).toBe("a-GQDCtt1Vk");
            });

            it("URL with timing", function() {
                var url = "https://youtu.be/Osck3M7ZvPc?t=7m12s";
                var result = KozYouTubeUtils.parseId(url);
                expect(result).toBe("Osck3M7ZvPc");
            });

        });


        describe("With text", function() {

        	it("Parse some text trying to find a video", function() {
	            var text = "A design pattern is the re-usable form of a solution to a design problem. For more information see: https://www.youtube.com/watch?time=1s&v=S-RjiMAxHio. Good luck!";
	            var result = KozYouTubeUtils.parseId(text);
	            expect(result).toBe("S-RjiMAxHio");
	        });

	        it("Parse some text with 2 links trying to find only the first video", function() {
	            var text = "A design pattern is the re-usable form of a solution to a design problem. For more information see: https://www.youtube.com/watch?time=1s&v=S-RjiMAxHio. Good luck! Oh, stop! One more link: https://youtu.be/MfnTJh-pQWM";
	            var result = KozYouTubeUtils.parseId(text);
	            expect(result).toBe("S-RjiMAxHio");
	        });

	        it("Parse some text which doesn't contain any YouTube links", function() {
	            var text = "A design pattern is the re-usable form of a solution to a design problem.";
	            var result = KozYouTubeUtils.parseId(text);
	            expect(result).toBe(false);
	        });

        });
	});


	describe("durationToString", function() {

		it("Convert PT5M40S into 05:40", function() {
            var time = "PT5M40S";
            var result = KozYouTubeUtils.durationToString(time);
            expect(result).toBe("05:40");
        });

        it("Convert PT15M5S into 15:05", function() {
            var time = "PT15M5S";
            var result = KozYouTubeUtils.durationToString(time);
            expect(result).toBe("15:05");
        });

        it("Convert PT1H30M0S into 01:30:00", function() {
            var time = "PT1H30M0S";
            var result = KozYouTubeUtils.durationToString(time);
            expect(result).toBe("01:30:00");
        });

        it("Convert PT1H30M0S into 01:30:00", function() {
            var time = "PT1H30M0S";
            var result = KozYouTubeUtils.durationToString(time);
            expect(result).toBe("01:30:00");
        });

        it("Convert P1DT3H13M47S into 01:03:13:47", function() {
            var time = "P1DT3H13M47S";
            var result = KozYouTubeUtils.durationToString(time);
            expect(result).toBe("01:03:13:47");
        });

	});

});