"use strict";

describe("Tests for KozExceptions", function() {
	
	describe("invalidArgument", function() {
		let wrapper = function(func) {
			return function() { func(42); };
		}

		it("No parameters", function() {
			let someFunc = function(x) {
				if (typeof(x) !== "string")
					throw new KozExceptions.invalidArgument();
			}

			expect(wrapper(someFunc)).toThrow(new KozExceptions.invalidArgument());

			try { someFunc(42); }
			catch (e) { expect(e.toString()).toBe('Invalid argument!'); }
		});

		it("Only argument", function() {
			let someFunc = function(x) {
				if (typeof(x) !== "string")
					throw new KozExceptions.invalidArgument("x");
			}

			expect(wrapper(someFunc)).toThrow(new KozExceptions.invalidArgument("x"));

			try { someFunc(42); }
			catch (e) { expect(e.toString()).toBe('Invalid argument: x!'); }
		});

		it("Argument and message", function() {
			let someFunc = function(x) {
				if (typeof(x) !== "string")
					throw new KozExceptions.invalidArgument("x", 'The argument "x" must be a string!');
			}

			expect(wrapper(someFunc)).toThrow(new KozExceptions.invalidArgument("x", 'The argument "x" must be a string!'));

			try { someFunc(42); }
			catch (e) { expect(e.toString()).toBe('Invalid argument: x! The argument "x" must be a string!'); }
		});
	});


	describe("missingArgument", function() {
		let wrapper = function(func) {
			return function() { func(); };
		}

		it("No parameters", function() {
			let someFunc = function(x) {
				if (x === undefined)
					throw new KozExceptions.missingArgument();
			}

			expect(wrapper(someFunc)).toThrow(new KozExceptions.missingArgument());

			try { someFunc(); }
			catch (e) { expect(e.toString()).toBe('Missing argument!'); }
		});

		it("Only argument", function() {
			let someFunc = function(x) {
				if (x === undefined)
					throw new KozExceptions.missingArgument("x");
			}

			expect(wrapper(someFunc)).toThrow(new KozExceptions.missingArgument("x"));

			try { someFunc(); }
			catch (e) { expect(e.toString()).toBe('Missing argument: x!'); }
		});

		it("Argument and message", function() {
			let someFunc = function(x) {
				if (x === undefined)
					throw new KozExceptions.missingArgument("x", 'The argument "x" is not passed!');
			}

			expect(wrapper(someFunc)).toThrow(new KozExceptions.missingArgument("x", 'The argument "x" is not passed!'));

			try { someFunc(); }
			catch (e) { expect(e.toString()).toBe('Missing argument: x! The argument "x" is not passed!'); }
		});
	});


	describe("notImplementedYet", function() {
		let wrapper = function(func) {
			return function() { func(); };
		}

		it("No parameters", function() {
			let someFunc = function() {
				throw new KozExceptions.notImplementedYet();
			}

			expect(wrapper(someFunc)).toThrow(new KozExceptions.notImplementedYet());

			try { someFunc(); }
			catch (e) { expect(e.toString()).toBe('Not implemented yet!'); }
		});
	});


	describe("abstractClass", function() {
		let wrapper = function(cls) {
			return function() { return new cls(); };
		}

		it("No parameters", function() {
			let someAbstractClass = function() {
				throw new KozExceptions.abstractClass();
			}

			expect(wrapper(someAbstractClass)).toThrow(new KozExceptions.abstractClass());

			try { new someAbstractClass(); }
			catch (e) { expect(e.toString()).toBe('You cannot create an instance of the abstract class!'); }
		});
	});


	describe("unresolvedDependency", function() {
		it("No KozMUL", function() {
			let wrapper = function() {
				return function() {
					if (typeof(KozMUL) == "undefined")
						throw new KozExceptions.unresolvedDependency('KozMUL');
				};
			}

			expect(wrapper()).toThrow(new KozExceptions.unresolvedDependency('KozMUL'));

			try { wrapper()(); }
			catch (e) { expect(e.toString()).toBe("Couldn't find a required library: KozMUL!"); }
		});
	});

});
