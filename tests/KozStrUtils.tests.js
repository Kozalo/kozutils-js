"use strict";

describe("Tests for KozStrUtils", function() {

	describe("matchAll", function() {
		let source = "Some sentence. [tag]Text in the first tag[/tag]. Another sentence. [tag]More words within the tag[/tag].";
		let expectedResults = [
			{ index: 15, string: "[tag]" },
			{ index: 41, string: "[/tag]" },
			{ index: 67, string: "[tag]" },
			{ index: 97, string: "[/tag]" }
		];

		it("RegExp", function() {
			let result = source.matchAll(/\[\/?tag\]/);

			for (let i in result) {
				expect(result[i].index).toBe(expectedResults[i].index);
				expect(result[i].string).toBe(expectedResults[i].string);
			}
		})

		it("String", function() {
			let result = source.matchAll("\\[\\/?tag\\]");

			for (let i in result) {
				expect(result[i].index).toBe(expectedResults[i].index);
				expect(result[i].string).toBe(expectedResults[i].string);
			}
		})
		
	});


	describe("shortenTo", function() {
		var text = "This is a very long string. I said: 'Very long!' Yeah! We need more letter here! Write! It's still a short string. Write more! Come on!";

        it("Shorten a long string to about 15 symbols (the count stops on the beginning of the word 'long')", function() {
            var result = text.shortenTo(15);
            expect(result).toBe("This is a very…");
        });

        it("Shorten a long string to about 50 symbols (the count stops on the word 'Yeah')", function() {
            var result = text.shortenTo(50);
            expect(result).toBe("This is a very long string. I said: 'Very long!'…");
        });

        it("Shorten a long string to about 58 symbols (the count stops on the end of the word 'We')", function() {
            var result = text.shortenTo(58);
            expect(result).toBe("This is a very long string. I said: 'Very long!' Yeah! We…");
        });

        it("Shorten a long string to about 80 symbols (the count stops on the end of the word 'here!')", function() {
            var result = text.shortenTo(80);
            expect(result).toBe("This is a very long string. I said: 'Very long!' Yeah! We need more letter here…");
        });

		it("Invalid parameter", function() {
			expect(function() { text.shortenTo(true); }).toThrow(new KozExceptions.invalidArgument("length", 'shortenTo: type of "length" must be: "number"!'));
		});
	});

});
