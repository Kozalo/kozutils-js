"use strict";

describe("Tests for KozUtils", function() {

	describe("initOptions", function() {
		let someFunc = function(options) {
			let o = KozUtils.initOptions(options, {
				a: 1,
				b: 2,
				c: 3
			});

			return o.a + o.b + o.c;
		}

		it("No options", function() {
			expect(someFunc()).toBe(6);
		});

		it('Only "a" and "b"', function() {
			expect(someFunc({a: 10, b: 20})).toBe(33);
		});

		it("All arguments", function() {
			expect(someFunc({a: 10, b: 20, c: 30})).toBe(60);
		});

		it("Redundant arguments", function() {
			let getObject = function(options) {
				let o = KozUtils.initOptions(options, {
					a: 1,
					b: 2
				});

				return o;
			}

			let o = getObject({a: 10, b: 20, c: 30});
			expect(o.a).toBe(10);
			expect(o.b).toBe(20);
			expect(o.c).toBe(30);
		});
	});


	describe("checkMissingArguments", function() {
		describe("Minimum information", function() {
			let wrappedFunc = function(a) { 
				let someFunc = function(a) {
					KozUtils.checkMissingParameters(a);
					return a;
				}

				return function() { someFunc(a); }
			}

			it("Without an argument", function() {
				let errorObj = {
					function: undefined,
					parameter: {
						index: 1,
						name: undefined
					}
				};
				expect(wrappedFunc()).toThrow(new KozExceptions.missingArgument(errorObj.parameter.name, JSON.stringify(errorObj)));
			});

			it("With an argument", function() {
				expect(wrappedFunc(1)).not.toThrow();
			});
		});

		describe("Index and function name", function() {
			let wrappedFunc = function(a) { 
				let someFunc = function(a) {
					KozUtils.checkMissingParameters(a, "wrappedFunc");
					return a;
				}

				return function() { someFunc(a); }
			}

			it("Without an argument", function() {
				let errorObj = {
					function: "wrappedFunc",
					parameter: {
						index: 1,
						name: undefined
					}
				};
				expect(wrappedFunc()).toThrow(new KozExceptions.missingArgument(errorObj.parameter.name, JSON.stringify(errorObj)));
			});

			it("With an argument", function() {
				expect(wrappedFunc(1)).not.toThrow();
			});
		});

		describe("All information", function() {
			let wrappedFunc = function(a) { 
				let someFunc = function(a) {
					KozUtils.checkMissingParameters(a, "wrappedFunc", "a");
					return a;
				}

				return function() { someFunc(a); }
			}

			it("Without an argument", function() {
				let errorObj = {
					function: "wrappedFunc",
					parameter: {
						index: 1,
						name: "a"
					}
				};
				expect(wrappedFunc()).toThrow(new KozExceptions.missingArgument(errorObj.parameter.name, JSON.stringify(errorObj)));
			});

			it("With an argument", function() {
				expect(wrappedFunc(1)).not.toThrow();
			});
		});

		describe("Some arguments and all information", function() {
			let wrappedFunc = function(a, b, c) { 
				let someFunc = function(a, b, c) {
					KozUtils.checkMissingParameters([a, b, c], "wrappedFunc", ['a', 'b', 'c']);
					return a + b + c;
				}

				return function() { someFunc(a, b, c); }
			}

			it("No arguments", function() {
				let errorObj = {
					function: "wrappedFunc",
					parameter: {
						index: 1,
						name: 'a'
					}
				};
				expect(wrappedFunc()).toThrow(new KozExceptions.missingArgument(errorObj.parameter.name, JSON.stringify(errorObj)));
			});

			it('Only "a" and "b"', function() {
				let errorObj = {
					function: "wrappedFunc",
					parameter: {
						index: 3,
						name: 'c'
					}
				};
				expect(wrappedFunc(1, 2)).toThrow(new KozExceptions.missingArgument(errorObj.parameter.name, JSON.stringify(errorObj)));
			});

			it("All arguments", function() {
				expect(wrappedFunc(1, 2, 3)).not.toThrow();
			});
		});
	});


	describe("checkArguments", function() {
		describe("Minimum information", function() {
			let wrappedFunc = function(a) { 
				let someFunc = function(a) {
					KozUtils.checkParameters(a, "string");
					return a;
				}

				return function() { someFunc(a); }
			}

			it("With a number argument", function() {
				expect(wrappedFunc(1)).toThrow(new KozExceptions.invalidArgument(undefined, 'String is expecting!'));
			});

			it("With a string argument", function() {
				expect(wrappedFunc("1")).not.toThrow();
			});
		});

		describe("Plus function name", function() {
			let wrappedFunc = function(a) { 
				let someFunc = function(a) {
					KozUtils.checkParameters(a, "string", "wrappedFunc");
					return a;
				}

				return function() { someFunc(a); }
			}

			it("With a number argument", function() {
				expect(wrappedFunc(1)).toThrow(new KozExceptions.invalidArgument(undefined, 'wrappedFunc: string is expecting!'));
			});

			it("With a string argument", function() {
				expect(wrappedFunc("1")).not.toThrow();
			});
		});

		describe("All information", function() {
			let wrappedFunc = function(a) { 
				let someFunc = function(a) {
					KozUtils.checkParameters(a, "string", "wrappedFunc", "a");
					return a;
				}

				return function() { someFunc(a); }
			}

			it("With a number argument", function() {
				expect(wrappedFunc(1)).toThrow(new KozExceptions.invalidArgument("a", 'wrappedFunc: type of "a" must be: "string"!'));
			});

			it("With a string argument", function() {
				expect(wrappedFunc("1")).not.toThrow();
			});
		});

		describe("Some arguments and all information", function() {
			let wrappedFunc = function(a, b, c) { 
				let someFunc = function(a, b, c) {
					KozUtils.checkParameters([a, b, c], ["number", "number", "number"], "wrappedFunc", ['a', 'b', 'c']);
					return a + b + c;
				}

				return function() { someFunc(a, b, c); }
			}

			it('String arguments', function() {
				expect(wrappedFunc("1", "2", "3")).toThrow(new KozExceptions.invalidArgument("a", 'wrappedFunc: type of "a" must be: "number"!'));
			});

			it("Valid arguments", function() {
				expect(wrappedFunc(1, 2, 3)).not.toThrow();
			});
		});
	});

});
