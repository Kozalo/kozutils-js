var gulp = require('gulp');
var uglifyjs = require('uglify-js-harmony');
var minifier = require('gulp-uglify/minifier');
var concat = require('gulp-concat');
var gutil = require('gulp-util');
var exec = require('child_process').exec;


gulp.task('default', function() {
    console.log("You can type 'gulp <taskname>' to use automatic build system.");
    console.log("There are some following tasks:");
    console.log("* build -- Minifies JS files and creates a build of the website in /_builds/%buildNumber% directory.");
    console.log("* build-dist -- Creates a final build of the website in the /dist directory.");
    console.log("* build-docs -- Runs JsDoc to make documentation.");
    console.log("* watch-docs -- Begins to watch for modified files in the /src directory and for the docs/jsdoc-readme.md file. If any file was changed, rebuild the documentation.");
});


gulp.task('build', ['build-docs'], function() {
    var currentDate = new Date();
    var dateStr = currentDate.getDate().toString() + currentDate.getMonth().toString() + currentDate.getYear().toString();
    var timeStr = currentDate.getHours().toString() + currentDate.getMinutes().toString() + currentDate.getSeconds().toString();
    var buildNumber = dateStr + '-' + timeStr;
    var buildPath = './_builds/' + buildNumber;

    makeBuild(buildPath);
});

gulp.task('build-dist', ['build-docs'], function() {
    var distPath = './dist/';

    if (process.platform === 'win32')
        exec('rmdir /s /q dist', (err, stdout, stderr) => {
            console.log(stderr);
        });
    else
        exec('rm /rf dist', (err, stdout, stderr) => {
            console.log(stderr);
        });

    setTimeout(function() { makeBuild(distPath); }, 1000);
});


gulp.task('build-docs', function() {
    exec('jsdoc -c ./docs/jsdoc-conf.json -R ./docs/jsdoc-readme.md', (err, stdout, stderr) => {
        console.log(stderr);
    });
});

gulp.task('watch-docs', function() {
    gulp.watch(['./src/*.js', './docs/jsdoc-readme.md'], ['build-docs']);
});


function makeBuild(buildPath) {
    gulp.src('./src/*.js')
        .pipe(minifier({compress: false}, uglifyjs)).on('error', gutil.log)
        .pipe(gulp.dest(buildPath));

    gulp.src('./src/*.js')
        .pipe(concat('KozUtils-AIO.js'))
        .pipe(minifier({compress: false}, uglifyjs)).on('error', gutil.log)
        .pipe(gulp.dest(buildPath));

    gulp.src('./docs/js/**/*.*')
        .pipe(gulp.dest(buildPath + '/docs'))
}
